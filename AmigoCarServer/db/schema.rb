# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171113163115) do

  create_table "reservations", force: :cascade do |t|
    t.integer "transaction_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["transaction_id"], name: "index_reservations_on_transaction_id"
    t.index ["user_id", "transaction_id"], name: "index_reservations_on_user_id_and_transaction_id", unique: true
    t.index ["user_id"], name: "index_reservations_on_user_id"
  end

  create_table "transactions", force: :cascade do |t|
    t.integer "user_id"
    t.string "dep_city"
    t.string "arrival_city"
    t.datetime "travel_date"
    t.integer "price"
    t.integer "nb_reservations"
    t.integer "nb_free_seats"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "smoking", default: false
    t.boolean "animals", default: false
    t.boolean "luggage", default: false
    t.string "car_type"
    t.boolean "cooling", default: false
    t.index ["user_id", "created_at"], name: "index_transactions_on_user_id_and_created_at"
    t.index ["user_id"], name: "index_transactions_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.boolean "admin", default: false
    t.string "telephone"
    t.string "first_name"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
