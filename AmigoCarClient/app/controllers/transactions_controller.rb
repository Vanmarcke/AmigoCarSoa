class TransactionsController < ApplicationController
  require 'savon'
  require 'will_paginate/array'
  require 'time'
  before_action :logged_in_user, only: %i[new create edit update]
  # before_action :correct_user,   only: %i[edit update]

  def new
    @user = session_user
    @transaction = Transaction.new({})
  end

  def index
    result = @client_transactions.call(:get_all_transactions, message: {
                                         'start_point' => params[:start_point],
                                         'end_point' => params[:end_point]
                                       })
    transactions_array = []
    hash_result = result.to_hash[:get_all_transactions_response]
    unless hash_result.nil?
      get_transactions_from_hash(hash_result[:transactions], transactions_array)
    end
    @transactions = transactions_array.paginate(page: params[:page], per_page: 15)
  end

  def show
    result = @client_transactions.call(:get_transaction_by_id, message: { id: params[:id] })
    hash_result = result.to_hash[:get_transaction_by_id_response]

    @transaction = Transaction.new(hash_result[:transaction])
    @user = User.new(hash_result[:user])
    @current_user = session_user
    @transaction.nb_free_seats = Integer(@transaction.nb_free_seats)
    @transaction.nb_reservations = Integer(@transaction.nb_reservations)
    passengers_array = []
    unless hash_result.nil?
      get_passengers_from_hash(hash_result[:passengers], passengers_array)
    end
    p passengers_array
    @passengers = passengers_array.paginate(page: params[:page], per_page: 15)
  end

  def create
    message_hash = transaction_params
    result = @client_transactions.call(:create_transaction, message: {
                                         transaction: {
                                           id: nil,
                                           'user_id' => message_hash[:user_id],
                                           'dep_city' => message_hash[:dep_city],
                                           'arrival_city' => message_hash[:arrival_city],
                                           'travel_date' => message_hash[:travel_date],
                                           price: message_hash[:price],
                                           'nb_reservations' => message_hash[:nb_reservations],
                                           'nb_free_seats' => message_hash[:nb_free_seats],
                                           'car_type' => message_hash[:car_type],
                                           luggage: message_hash[:luggage],
                                           smoking: message_hash[:smoking],
                                           animals: message_hash[:animals],
                                           cooling: message_hash[:cooling]
                                         }
                                       })
    id = result.to_hash[:create_transaction_response][:value]
    if id
      flash[:success] = 'Success!'
      redirect_to transaction_url(id)
    else
      flash[:danger] = 'Failure?!'
      render 'new'
    end
  end

  #
  #
  # NOT CHANGED
  #
  #

  def edit
    @user = current_user
    @transaction = Transaction.find(params[:id])
  end

  def update
    @transaction = Transaction.find(params[:id])
    if @transaction.update_attributes(transaction_params)
      flash[:success] = 'Travel updated'
      redirect_to @transaction
    else
      render 'edit'
    end
  end

  private

  def logged_in_user
    unless logged_in?
      flash[:danger] = 'Please log in.'
      redirect_to login_url
    end
  end

  # Confirms the correct user.
  def correct_user
    @transaction = Transaction.find(params[:id])
    @user = User.find(@transaction.user_id)
    unless current_user?(@user)
      flash[:danger] = 'Unauthorized action !'
      redirect_to(root_url)
    end
  end

  def transaction_params
    params.permit(:dep_city, :arrival_city, :nb_free_seats,
                                        :travel_date, :price, :user_id, :nb_reservations,
                                        :luggage, :smoking, :animals, :car_type, :cooling,
                                        :created_at, :updated_at)
  end
end
