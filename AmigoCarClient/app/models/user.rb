class User
  extend ActiveModel::Model
  # has_many :transactions
  # has_many :reservations

  # before_save { self.email = email.downcase }

  attr_accessor :id,
                :name,
                :first_name,
                :telephone,
                :email,
                :password_digest,
                :created_at,
                :updated_at,
                :admin

  def initialize(hash)
    @id = hash[:id]
    @name = hash[:name] unless hash[:name].nil?
    @first_name = hash[:first_name] unless hash[:first_name].nil?
    @telephone = hash[:telephone] unless hash[:telephone].nil?
    @email = hash[:email] unless hash[:email].nil?
    @password_digest = hash[:password_digest] unless hash[:password_digest].nil?
    @created_at = hash[:created_at] unless hash[:created_at].nil?
    @updated_at = hash[:updated_at] unless hash[:updated_at].nil?
    @admin = hash[:admin] unless hash[:admin].nil?
  end


  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
               BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def authenticate(unencrypted_password)
    BCrypt::Password.new(password_digest).is_password?(unencrypted_password) && self
  end
end
