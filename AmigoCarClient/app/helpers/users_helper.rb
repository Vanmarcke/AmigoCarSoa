module UsersHelper

#  def gravatar_for(user,options = { size: 80 })
#    puts user
#    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
#    size = options[:size]
#    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}&d=identicon"
#    image_tag(gravatar_url, alt: user.name, class: "gravatar")
#  end

  def get_transactions_from_hash(hash_result, transactions_array)
    unless hash_result.nil?
      if hash_result.instance_of?(Hash)
        @transaction = Transaction.new(hash_result)
        transactions_array << @transaction
      else
        hash_result.each do |transaction|
          @transaction = Transaction.new(transaction)
          transactions_array << @transaction
        end
      end
    end
  end

end
