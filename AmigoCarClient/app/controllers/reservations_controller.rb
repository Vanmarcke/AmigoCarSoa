class ReservationsController < ApplicationController
  require 'savon'

  def show
    result = @client_reservations.call(:get_reservation_by_id, message: { id: params[:id] })
    @reservation = Reservation.new(result.to_hash[:get_reservation_by_id_response][:reservation])
    @transaction = Transaction.new(result.to_hash[:get_reservation_by_id_response][:transaction])
  end

  def create
    @user_id = session_user.id

    message_hash = reservation_params
    result = @client_reservations.call(:create_reservation, message: {
                                         reservation: {
                                           id: nil,
                                           "user_id" => @user_id,
                                           "transaction_id" => message_hash[:transaction_id],

                                         },
                                         passenger: params[:seats_selected]
                                       })

    id = result.to_hash[:create_reservation_response][:value]
    p id
    if id
      flash[:success] = 'Your reservation was confirmed!'
      redirect_to reservation_url(id)
    else
      flash[:danger] = 'An error occurred !'
      redirect_to transaction_url(message_hash[:id])
    end
  end

  private

  def reservation_params
    params.permit(:id, :user_id, :transaction_id)
  end
end
