Rails.application.routes.draw do
  AmigoCarServer::Application.routes.draw do
    wash_out :users
    wash_out :reservations
    wash_out :transactions
  end

  root 'users#home'

end
