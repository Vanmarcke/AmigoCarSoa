module TransactionsHelper

  def get_passengers_from_hash(hash_result, passengers_array)
    unless hash_result.nil?
      if hash_result.instance_of?(Hash)
        @passenger = User.new(hash_result)
        passengers_array << @passenger
      else
        hash_result.each do |user|
          @passenger = User.new(user)
          passengers_array << @passenger
        end
      end
    end
  end
end
