class User < ApplicationRecord

  map id: :integer,
      name: :string,
      first_name: :string,
      telephone: :string,
      email: :string,
      password_digest: :string,
      admin: :boolean

  has_many :transactions
  has_many :reservations

  before_save { self.email = email.downcase }

  VALID_NAME_REGEX = /\A[\w+\-.']+\z/
  validates :name, presence: true, length: { maximum: 25 },
                   uniqueness: { case_sensitive: false }, format: { with: VALID_NAME_REGEX }

  validates :first_name, length: { maximum: 25 }, format: { with: VALID_NAME_REGEX }, allow_blank:

  VALID_TELEPHONE_REGEX = /\A\d{3}[\-\\.]\d{3}[\-\\.]\d{4}\z/
  validates :telephone, format: { with: VALID_TELEPHONE_REGEX }, allow_blank: true

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }

  # has_secure_password

  VALID_PASSWORD_REGEX = /\A[\w+\-.]+\z/
  validates :password_digest, presence: true, length: { maximum: 255 }
end



