class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  include UsersHelper
  include TransactionsHelper

  before_action :init_client

  private

  def init_client
    @client_users = Savon::Client.new(wsdl: 'http://localhost:3000/users/wsdl')
    @client_transactions = Savon::Client.new(wsdl: 'http://localhost:3000/transactions/wsdl')
    @client_reservations = Savon::Client.new(wsdl: 'http://localhost:3000/reservations/wsdl')
  end


end
