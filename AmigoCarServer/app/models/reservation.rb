class Reservation < ApplicationRecord
  map id: :integer,
      user_id: :integer,
      transaction_id: :integer

  belongs_to :user

  validates :user_id, presence: true
  validates :transaction_id, presence: true
end
