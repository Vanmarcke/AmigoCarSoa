class Transaction
  extend ActiveModel::Model
  # belongs_to :user

  attr_accessor :id,
                :user_id,
                :dep_city,
                :arrival_city,
                :travel_date,
                :price,
                :nb_reservations,
                :nb_free_seats,
                :car_type,
                :luggage,
                :smoking,
                :animals,
                :cooling,
                :created_at,
                :updated_at

  def initialize(hash)
    @id = hash[:id]
    @user_id = hash[:user_id]
    @dep_city = hash[:dep_city]
    @arrival_city = hash[:arrival_city]
    @travel_date = hash[:travel_date]
    @price = hash[:price]
    @nb_reservations = hash[:nb_reservations]
    @nb_free_seats = hash[:nb_free_seats]
    @car_type = hash[:car_type]
    @luggage = hash[:luggage]
    @smoking = hash[:smoking]
    @animals = hash[:animals]
    @cooling = hash[:cooling]
    @created_at = hash[:created_at] unless hash[:created_at].nil?
    @updated_at = hash[:updated_at] unless hash[:updated_at].nil?
  end
end
