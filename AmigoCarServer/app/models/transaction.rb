class Transaction < ApplicationRecord
  searchkick
  belongs_to :user
  # has_many :reservations

  map id: :integer,
      user_id: :integer,
      dep_city: :string,
      arrival_city: :string,
      travel_date: :datetime,
      price: :integer,
      nb_reservations: :integer,
      nb_free_seats: :integer,
      car_type: :string,
      luggage: :boolean,
      smoking: :boolean,
      animals: :boolean,
      cooling: :boolean,
      created_at: :datetime,
      updated_at: :datetime


  default_scope -> { order(:travel_date) }
  validates :user_id, presence: true
  validates :dep_city, presence: true
  validates :arrival_city, presence: true
  validates :travel_date, presence: true
  validates :price, presence: true
  validates :nb_reservations, presence: true
  validates :nb_free_seats, presence: true
  validates :car_type, presence:true

end
