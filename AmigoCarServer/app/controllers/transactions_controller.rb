class TransactionsController < ApplicationController
  soap_service namespace: 'urn:WashOut'

  soap_action 'get_all_transactions',
              args: { start_point: :string, end_point: :string },
              return: { transactions: [Transaction] },
              to: :index
  def index
    @transactions = Transaction.all
    if !params[:start_point].blank? && !params[:end_point].blank?
      @transactions = Transaction.search params[:start_point],
                                         where: { arrival_city: params[:end_point] },
                                         page: params[:page],
                                         per_page: 15,
                                         fields: [:dep_city]
    elsif !params[:start_point].blank? && params[:end_point].blank?
      @transactions = Transaction.search params[:start_point],
                                         page: params[:page],
                                         per_page: 15,
                                         fields: [:dep_city]
    elsif params[:start_point].blank? && !params[:end_point].blank?
      @transactions = Transaction.search params[:end_point],
                                         page: params[:page],
                                         per_page: 15,
                                         fields: [:arrival_city]
    end
    transactions_array = []
    @transactions.each do |transaction|
      transactions_array << transaction.attributes
    end
    render soap: { transactions: transactions_array }
  end

  soap_action 'get_transaction_by_id',
              args: { id: :integer },
              return: { transaction: Transaction, user: User, passengers: [User] },
              to: :show
  def show
    @transaction = Transaction.find(params[:id])
    @reservations = Reservation.where(transaction_id: @transaction.id).all
    passengers_array = []
    @reservations.each do |reservation|
      @user = User.find(reservation.user_id)
      passengers_array << @user.attributes
    end
    render soap: { transaction: @transaction.attributes, user: @transaction.user.attributes,
                   passengers: passengers_array }
  end

  soap_action 'create_transaction',
              args: { transaction: Transaction },
              return: :integer,
              to: :create
  def create
    @transaction = Transaction.new(transaction_params)
    if @transaction.save
      render soap: @transaction.id
    else
      render soap: nil
    end
  end

  def edit
    @user = current_user
    @transaction = Transaction.find(params[:id])
  end

  def update
    @transaction = Transaction.find(params[:id])
    if @transaction.update_attributes(transaction_params)
      flash[:success] = 'Travel updated'
      redirect_to @transaction
    else
      render 'edit'
    end
  end

  private

  def logged_in_user
    unless logged_in?
      flash[:danger] = 'Please log in.'
      redirect_to login_url
    end
  end

  # Confirms the correct user.
  def correct_user
    @transaction = Transaction.find(params[:id])
    @user = User.find(@transaction.user_id)
    unless current_user?(@user)
      flash[:danger] = 'Unauthorized action !'
      redirect_to(root_url)
    end
  end

  def transaction_params
    params.require(:transaction).permit(:dep_city, :arrival_city, :nb_free_seats,
                                        :travel_date, :price, :user_id, :nb_reservations,
                                        :luggage, :smoking, :animals, :car_type, :cooling)
  end
end
