class UsersController < ApplicationController
  soap_service namespace: 'urn:WashOut'

  soap_action 'get_user_by_id',
              args: { id: :integer },
              return: { user: User, transactions: [Transaction], passenger_transactions: [Transaction] },
              to: :show
  def show
    @user = User.find(params[:id])
    # Transactions as driver
    @transactions = @user.transactions
    transactions_array = []
    @transactions.each do |transaction|
      transactions_array << transaction.attributes
    end
    # Transactions as passenger
    @reservations = Reservation.where({ :user_id => @user.id }).all
    @ids = []
    @reservations.each do |reservation|
      @ids << reservation.transaction_id
    end
    @passenger_transactions = Transaction.where(id: @ids )
    pass_transactions_array = []
    @passenger_transactions.each do |transaction|
      pass_transactions_array << transaction.attributes
    end
    render soap: { user: @user.attributes,
                   transactions: transactions_array,
                   passenger_transactions: pass_transactions_array }
  end



  soap_action 'get_user_by_email',
              args: { email: :string },
              return: { user: User },
              to: :show_email
  def show_email
    @user = User.find_by(email: params[:email])
    render soap: { user: @user.attributes }
  end

  soap_action 'get_all_users',
              args: nil,
              return: { users: [User] },
              to: :index
  def index
    @users = User.all
    users_array = []
    @users.each do |user|
      users_array << user.attributes
    end
    render soap: { users: users_array }
  end

  soap_action 'create_user',
              args: { user: User },
              return: :integer,
              to: :create
  def create
    @user = User.new(user_params)
    if @user.save
      render soap: @user.id
    else
      render soap: nil
    end
  end

  soap_action 'update_user',
              args: {user: User},
              return: :integer,
              to: :update

  def update
    @user = User.find(user_params[:id])
    if @user.update_attributes(user_params)
      render soap: @user.id
    else
      render soap: nil
    end
  end



  private
  def user_params
    params.require(:user).permit(:id,:name, :first_name, :telephone, :email, :password_digest)
  end
end


