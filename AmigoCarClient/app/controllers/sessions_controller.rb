class SessionsController < ApplicationController
  before_action :init_client

  def new
  end

  def create
    result = @client_users.call(:get_user_by_email,
                          message: { email: params[:session][:email].downcase })
    user = User.new(result.to_hash[:get_user_by_email_response][:user])
    if user && user.authenticate(params[:session][:password])
      log_in user
      redirect_to user_url(user.id)

    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end
end
