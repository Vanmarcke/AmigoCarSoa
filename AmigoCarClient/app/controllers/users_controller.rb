class UsersController < ApplicationController
  require 'savon'
  require 'will_paginate/array'

  before_action :logged_in_user, only: [:index, :edit, :update,:destroy]
  # before_action :correct_user,   only: [:edit, :update]



  def new
    @user = User.new({})
  end

  def edit
    @user = session_user
  end

  def index
    result = @client_users.call(:get_all_users)
    temp = []
    result.to_hash[:get_all_users_response][:users].each do |user|
      @user = User.new(user)
      temp.push(@user)
    end
    p temp
    @users = temp.paginate(page: params[:page])
  end

  def show
    result = @client_users.call(:get_user_by_id, message: { id: params[:id] })
    @user = User.new(result.to_hash[:get_user_by_id_response][:user])

    transactions_array = []
    passenger_transactions_array = []
    hash_result = result.to_hash[:get_user_by_id_response]
    unless hash_result.nil?
      get_transactions_from_hash(hash_result[:transactions], transactions_array)
      get_transactions_from_hash(hash_result[:passenger_transactions], passenger_transactions_array)
    end
    @transactions = transactions_array.paginate(page: params[:page], per_page: 15)
    @reservations = passenger_transactions_array.paginate(page: params[:page], per_page: 15)
  end

  def create
    if params[:password_confirmation] != params[:password]
      flash[:danger] = 'Password and confirmation are differents'
      redirect_to new_user_path
    else
      message_hash = user_params
      message_hash[:password_digest] = User.digest(message_hash[:password])
      result = @client_users.call(:create_user, message: {
                                    user: {
                                      id: nil,
                                      name: message_hash[:name],
                                      'first_name' => message_hash[:first_name],
                                      telephone: message_hash[:telephone],
                                      email: message_hash[:email],
                                      'password_digest' => message_hash[:password_digest]
                                    }
                                  })
      id = result.to_hash[:create_user_response][:value]
      if id
        @user = User.new(id: id, name: message_hash[:name],
                         first_name: message_hash[:first_name])
        log_in @user
        flash[:success] = 'Welcome to AmigoCar!'
        redirect_to user_url(id)
      else
        flash[:danger] = 'An error occured !'
        redirect_to new_user_path
      end
    end
  end

  def update
    if params[:password_confirmation] != params[:password]
      flash[:danger] = 'Password and confirmation are differents'
      redirect_to edit_user_path
    else
      message_hash = user_params
      message_hash[:password_digest] = User.digest(message_hash[:password])
      result = @client_users.call(:update_user, message: {
                                    user: {
                                      id: message_hash[:id],
                                      name: message_hash[:name],
                                      'first_name' => message_hash[:first_name],
                                      telephone: message_hash[:telephone],
                                      email: message_hash[:email],
                                      'password_digest' => message_hash[:password_digest]
                                    }
                                  })
      id = result.to_hash[:update_user_response][:value]
      if id
        flash[:success] = 'Profile updated'
        redirect_to user_url(id)
      else
        flash[:danger] = 'An error occurred !'
        redirect_to edit_user_path
      end

    end
  end

  private

  def logged_in_user
    unless logged_in?
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end

  # Confirms the correct user.
  def correct_user
    @user = User.find(params[:id])
    unless current_user?(@user)
      flash[:danger] = "Unauthorized action !"
      redirect_to(root_url)
    end
  end

  def user_params
    params.permit(:id, :name, :first_name, :email, :password, :password_confirmation, :telephone, :commit, :authenticity_token, :utf8)
  end
end
