module SessionsHelper
  # Logs in the given user.
  def log_in(user)
    session[:user_id] = user.id
    session[:user_name] = user.name
    session[:user_first_name] = user.first_name
  end

  # Returns the current logged-in user (if any).
  def current_user
    if session[:user_id]
      result = @client.call(:get_user_by_id, message: { id: session[:user_id] })
      @current_user ||= User.new(result.to_hash[:get_user_by_id_response][:user])
    else
      @current_user = nil
    end
  end

  # Returns the current logged-in user (if any) with session information
  # to avoid latency due to soap operations
  def session_user
    if session[:user_id] && session[:user_name] && session[:user_first_name]
      @session_user = User.new({name: session[:user_name], first_name: session[:user_first_name], id: session[:user_id] })
    else
      @session_user = nil
    end
  end

  # Returns true if the given user is the current user.
  def current_user?(user)
    user.id == session[:user_id]
  end

  # Returns true if the user is logged in, false otherwise.
  def logged_in?
    !session_user.nil?
  end

  # Logs out the current user.
  def log_out
    session.delete(:user_id)
    session.delete(:user_name)
    session.delete(:user_first_name)
    @current_user = nil
  end
end
