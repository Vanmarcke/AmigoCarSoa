# AmigoCar Project - SOA 

## Notre projet

- Reprendre le site AmigoCar réalisé [précedemment](https://gitlab.com/Vanmarcke/AmigoCar "") qui a une architecture ROA, afin de l’orienter service. 
- Le développer avec les mêmes outils (Ruby on Rails) même si cela implique des difficultés supplémentaires



## Gem utilisées

- [SAVON](https://github.com/savonrb "") : pour la consommation des services 
- [WASH_OUT](https://github.com/inossidabile/wash_out "") : pour exposer nos services


