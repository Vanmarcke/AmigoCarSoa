class ReservationsController < ApplicationController
  soap_service namespace: 'urn:WashOut'

  soap_action 'get_reservation_by_id',
              args: { id: :integer },
              return: { reservation: Reservation, transaction: Transaction },
              to: :show
  def show
    @reservation = Reservation.find(params[:id])
    @transaction = Transaction.find(@reservation.transaction_id)
    render soap: { reservation: @reservation.attributes,
                   transaction: @transaction.attributes }
  end

  soap_action 'create_reservation',
              args: {reservation: Reservation, passenger: :integer},
              return: :integer,
              to: :create

  def create
    @reservation = Reservation.new(reservation_params)
    @transaction = Transaction.find(reservation_params[:transaction_id])

    if @reservation.save
      @transaction.update_attributes!(nb_reservations: (@transaction.nb_reservations + Integer(params[:passenger])),
                                      nb_free_seats: (@transaction.nb_free_seats - Integer(params[:passenger])))
      render soap: @reservation.id
    else
      render soap: nil
    end
  end

  private
  def reservation_params
    params.require(:reservation).permit(:id, :user_id, :transaction_id)
  end

end
